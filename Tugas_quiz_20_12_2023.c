#include <stdio.h>
#include <stdlib.h>

void jdl_aplikasi();
void input(char *nama, char *gol, char *status);
void process(char gol, char *status, float *gapok, float *tunja, float *prosen_pot, float *pot, float *gaber);
void output(float gapok, float tunja, float pot, float gaber);

int main()
{


    char nama[50];
    char gol = ' ';
    char status[10];

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    printf("*************************\n");
    printf("*  Aplikasi Hitung Gaji *\n");
    printf("*************************\n\n");

    jdl_aplikasi(); // Call your empty function

    input(nama, &gol, status);
    process(gol, status, &gapok, &tunja, &prosen_pot, &pot, &gaber);
    output(gapok, tunja, pot, gaber);

    return 0;
}

void jdl_aplikasi()
{
    // Add any specific tasks for this function if needed
}

void input(char *nama, char *gol, char *status)
{
    printf("Nama Karyawan       : ");
    scanf("%s", nama);
    printf("Golongan (A\\B)      : ");
    scanf(" %c", gol);
    printf("Status (Nikah\\Belum): ");
    scanf("%s", status);
}

void process(char gol, char *status, float *gapok, float *tunja, float *prosen_pot, float *pot, float *gaber)
{
    switch (gol)
    {
    case 'A':
        *gapok = 200000;
        if (strcmp(status, "Nikah") == 0)
        {
            *tunja = 50000;
        }
        else
        {
            if (strcmp(status, "Belum") == 0)
            {
                *tunja = 25000;
            }
        }
        break;
    case 'B':
        *gapok = 350000;
        if (strcmp(status, "Nikah") == 0)
        {
            *tunja = 75000;
        }
        else
        {
            if (strcmp(status, "Belum") == 0)
            {
                *tunja = 60000;
            }
        }
        break;
    default:
        printf(" Salah Input Golongan !!!\n");
        exit(1);
    }

    if (*gapok > 300000)
    {
        *prosen_pot = 0.1;
    }

    *pot = (*gapok + *tunja) * *prosen_pot;
    *gaber = (*gapok + *tunja) - *pot;
}

void output(float gapok, float tunja, float pot, float gaber)
{
    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);
}
