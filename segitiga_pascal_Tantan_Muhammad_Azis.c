#include <stdio.h>
int calculateCombination(int n, int r) {
if (r == 0 || r == n) {
return 1;
} else {
return calculateCombination(n - 1, r - 1) + calculateCombination(n -1, r);
}
}
int main() {
int N, i, j;
printf("Masukkan N: ");
scanf("%d", &N);
for (i = 0; i < N; i++) {
for (j = 0; j < N - i - 1; j++) {
printf(" ");
}
for (j = 0; j <= i; j++) {
printf("%d ", calculateCombination(i, j));
}
printf("\n");
}
return 0;
}
/*
STEP-1 (SEGITIGA PASCAL (BINTANG))
Masukkan N: 5
     * 
    * * 
   * * * 
  * * * * 
 * * * * * 
* * * * * * 

STEP-2 (MENGUBAH BINTANG MENJADI ANGKA)
Masukkan N: 5
    1 
   1 2 
  1 2 3 
 1 2 3 4 
1 2 3 4 5 

STEP-3 (KOMBINASI DAN FAKTORIAL)
Masukkan N: 5
    0C0=1 1! 
   1C0=1 1! 1C1=1 1! 
  2C0=1 1! 2C1=2 1! 2C2=1 2! 
 3C0=1 1! 3C1=3 1! 3C2=3 2! 3C3=1 6! 
4C0=1 1! 4C1=4 1! 4C2=6 2! 4C3=4 6! 4C4=1 24! 

STEP-4 (HASIL AKHIR)
Masukkan N: 5
    1 
   1 1 
  1 2 1 
 1 3 3 1 
1 4 6 4 1
*/
