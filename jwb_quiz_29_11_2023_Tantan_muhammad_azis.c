#include <stdio.h>

int main()
{


int i=0, N=0;
float Data=0.0, Rata=0.0, Total=0.0;

do {
printf("Masukan Data ke-%d : ", i + 1);
scanf("%f", &Data);

if (Data == 0) {
break;
}

Total += Data;
i++;
} while (1);

N = i; 

Rata = Total / N;
printf("Banyaknya Data : %d\n", N);
printf("Total Nilai Data : %.2f\n", Total);
printf("Rata-rata nilai Data : %.2f\n", Rata);

return 0;
}
/*
Masukan Data ke-1 : 1
Masukan Data ke-2 : 2
Masukan Data ke-3 : 3
Masukan Data ke-4 : 0
Banyaknya Data : 3
Total Nilai Data : 6.00
Rata-rata nilai Data : 2.00
*/
